docker-shopware
==============



### THIS IS FOR DEVELOPMENT ENVIRONMENT ONLY! THIS IS NOT SUITED FOR PRODUCTION ENVIRONMENT!

# Installation

First, clone this repository:

```bash
$ git clone git@gitlab.com:k.stanelis/docker-shopware.git
```

Shopware 6 project should go to `src` folder. To define custom address edit `ServerName` in `docker/apache/config/my-httpd.conf` ath the end of file inside `<VirtualHost *:80>`.

In hosts file add `shopware.test 127.0.0.1`

Then, run:

```bash
$ docker-compose up
```

You are done, you can visit your Shopware 6 application on the following URL: `http://shopware.test`

# Use xdebug!

Configure your IDE to use port 5902 for XDebug.
Docker versions below 18.03.1 don't support the Docker variable `host.docker.internal`.  
In that case you'd have to swap out `host.docker.internal` with your machine IP address in php-fpm/xdebug.ini.
For CLI debuging for DB connection use docker local IP. You can get it from phpinfo() `$_SERVER['REMOTE_ADDR']`.

# Code license

You are free to use the code in this repository under the terms of the 0-clause BSD license. LICENSE contains a copy of this license.
